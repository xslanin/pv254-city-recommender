# Holiday destinations (PV254 Project)

1. Motivation / Idea
2. Dataset
3. Data analysis, feature engineering
3. Recommendations
4. Application
5. Testing
6. Conclusion

####  Team members and Responsibilities:
- T. Slanináková (445526)
    - Data scraping, recommendation algorithms, testing
- J. Vrbka (TODO)
    - Data scraping, recommendation algorithms, Server UI
- M. Bažík (445446) 
    - Data visualization, analysis, algorithms
- M. Horňák (TODO) 
    - Server

## 1. Motivation / Idea
We're recommending cities to visit as travel destinations based on person's travel history and other preferences.


- Use case: I, as a user, want to find interesting cities to visit based on cities I liked in the past.

## 2. Dataset
- 2 datasets: `trips`, `cities`
- both scraped from nomadlist.com
- 72k trips, 3700 users and 599 cities.

![nomad_city](./nomad_city.png)

![nomad_user](./nomad_user.png)

![df_trips](df_trips.png)

![df_cities](df_cities.png)
![df_cities_cols](df_cities_cols.png)

## 3. Data
### Data analysis, feature engineering, vizualization

Incomplete data:
- constant (-1, not suitable for distances) (former)
- remove incomplete items (would lose cities)
- summarize column (mean, median, most frequent)
- summarize items features (iterative imputation)

![png](output_19_0.png)


Outliers: 
- "mars-city" :) (high cost of living)
- histogram after removal of Mars
- cost of living is not normally distributed (binning, scaling)

![png](output_23_1.png)


### Mean imputing strategy for missing values

Substitutes missing values for a feature mean.

```python
from sklearn.impute import SimpleImputer
imp = SimpleImputer(strategy='mean', missing_values=-1)
```

### Iterative imputing strategy for missing values

Firstly we scale cost and internet to (1,5) interval of the other features, so it does not influence the data imputation. Large values forces imputer to overfit on this value. 


```python
# Enables experimental imputation of the data.
from sklearn.experimental import enable_iterative_imputer
from sklearn.impute import IterativeImputer

imp = IterativeImputer(max_iter=10, random_state=0, missing_values=-1)
```

### Histograms after the iterative imputation

- mostly similar to normal distribution
- places_to_walk_from, walkability and traffic safety have a single dominant value
- zero values in hospital and female friendly attributes (not present previously)

![png](output_34_0.png)


### Covariance matrix

- needs scaling of internet speed and cost of living
- similar places_to_work_from/walkability, female_friendly/lgbt_friendly, nightlife/fun, and racial_tolerance/freedom_of_speech
- not significant
- the similarities among them somehow makes sense


![png](output_38_1.png)


### Corellation matrix

- female_friendly/lgbt_friendly with correlation 0.57 (covariance)
- The most significant correlation 0.67 startup_score and quality_of_life 
- Correlation between fun and nightlife resent, but only 0.41
- None was significant enough to remove a feature. 
- Gave some insight


```python
corrmat = df_cities.corr()
```


![png](output_41_1.png)


## KNN clustering

- Similarity between the cities is in our algorithms defined by the distance between them
- Information about the structure of data. 
- Clusters simulate distance based recommendations
- Use for recommending similar cities, but not the most similar
- Example: KNN, 10 clusters
- It does not correlate with any attribute 

    hospital_availability        -0.027889
    nightlife                    -0.032095
    ...
    cost_dollars_per_month       -0.106663
    labels                        1.000000



### PCA

- Reduced the data into 2 dimensions using PCA 
- Interesting properties
- Clusters were easily identifiable and depended simply on the pricipal component 
- High values are present due to high values of cost_dollars_per_month

![png](output_47_1.png)


- Clusters are mainly of the similar cost. So binning or scaling may be a good idea.


<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>principal component 1</th>
      <th>principal component 2</th>
      <th>city</th>
      <th>hospital_availability</th>
      <th>nightlife</th>
      <th>...</th>
      <th>internet_mbs</th>
      <th>cost_dollars_per_month</th>
      <th>labels</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>674</th>
      <td>5305.398349</td>
      <td>-21.957519</td>
      <td>key-west</td>
      <td>3.000000</td>
      <td>2.0</td>
      <td>...</td>
      <td>33.0</td>
      <td>7789.0</td>
      <td>3</td>
    </tr>
    <tr>
      <th>368</th>
      <td>4406.288700</td>
      <td>-36.180822</td>
      <td>mykonos</td>
      <td>3.344372</td>
      <td>4.0</td>
      <td>...</td>
      <td>13.0</td>
      <td>6890.0</td>
      <td>3</td>
    </tr>
    <tr>
      <th>169</th>
      <td>4286.496326</td>
      <td>-3.351485</td>
      <td>monaco</td>
      <td>3.000000</td>
      <td>4.0</td>
      <td>...</td>
      <td>45.0</td>
      <td>6770.0</td>
      <td>3</td>
    </tr>
    
  </tbody>
</table>
<p>10 rows × 23 columns</p>
</div>


If we scale during fitting PCA. Principal components look differently and clusters are visible, but not that much.


![png](output_53_1.png)


## 3. Recommendation algorithms

### 1. Distance-based approaches

### How to find similar cities: Distance-based techniques
- 7 distance measurement techniques used: `'euclidean', 'minkowski', 'braycurtis', 'hamming', 'l1', 'manhattan', 'cosine'`
- work only with `cities` dataset

### How to find which cities are being visited together (by 1 user): Similarity coefficient
- works with `trips` dataset
$$sim(A,B) = \frac{count(A,B)}{count(A)*count(B)}$$
where A,B are cities and count(A) represents number of occurences of A in the dataset

### 2. User-similarity-based: TripSimilarity
1. Groups cities based on user, removed duplicities
2. Finds groups of users that satisfy user input (biggest intersection)
3. Finds cities with the most occurrences
4. Shuffles and outputs the first `n`

### 3. k-NN
1. Impute missing values with most_frequent
2. Custer cities based on thei features into 120 clusters
3. Sample `n` random cities of the cluster of selected cities

## 4. Application
- Backend, data-handling using Python
- Frontend using Python (Jinja), HTML, CSS, JS, Bootstrap
- SQLite database
- we tried to deploy on Heroku, ran into RAM limitations

![app_1](./app_1.png)

![app_2](./app_2.png)

## 5. Testing + Results
### Alpha testing

#### Scenarios:
| Name               | Expectation |  |
|------------------------|---------------| -- | 
| one_generic (london)      | Other generic cities      |  |
| more_generic      | Other generic cities      |  |
| only_europe      | Other cities in Europe?    |  |
| expensive_cities      |  Other expensive cities    |  |
| regional      |  Other regionally close cities  |  |
| sea | Other sea destinations |  |
| historical | Other historial (e.g. Athens) |  |
| mountains | Other cities typ. of mountain tourism | |
| cold | Other cities typ. of winter tourism | |
| asia | Other cities in asia | |

#### Testing scenarios:
- distance metrics the same result, hamming and cosine slightly different result

| Name               | Expectation | Actual result (euclidean, ...) | Actual result (hamming) | Actual result (cosine) |
|------------------------|---------------|---|---|---|
| london      | Other generic cities like London     | paris, milan, **whistler, cody, cheltenham** | **edinburgh**, ibiza, nice,  **fort-lauderdale, lille** | tel-aviv, **haifa, santorini, oxford, leeds**

#### imputing and binning, use with similarity coefficient helped

| Name               | Expectation | Actual result (euclidean, ...) | Actual result (hamming) | Actual result (cosine) |
|------------------------|---------------|---|---|---|
| london      | Other generic cities like London     | paris, milan, **whistler, cody, cheltenham** | **edinburgh**, ibiza, nice,  **fort-lauderdale, lille** | tel-aviv, **haifa, santorini, oxford, leeds**

| Name               | Expectation | Act. euclidean+ binned Sim. Coeff | Act. hamming binned Sim. Coeff | Act. cosine binned Sim. Coeff | 
|------------------------|---------------|---|---|---|
| london      | Other generic cities like London     | oxford, tel-aviv, miami, leeds, LA | key-west, ibiza, willemstad, tel-aviv, bordeaux | oxford, tel-aviv, miami, leeds, LA |

### Final algorithms chosen for beta testing:
- k-NN (on trips, 120 neighbors), 
- SVD (from the proof-of-concept phase) 
- Random (giving random predictions, as a control)
- Cosine Similarity with Similarity Coefficient with raw data
- Hamming distance with Similarity Coefficient with imputed and binned data


### Testing on final algorithms with use cases

![final_alpha_test_1](final_alpha_test_1.png)

![final_alpha_test_2.png](final_alpha_test_2.png)



### Beta testing

| Subject | Input | k-NN | SVD | Random | Cosine + SimCoeff, Raw data | TripSimilarity | Hamming + SimCoeff, binned imputed |
|------------------------|---------------|---|---|---|---|---|---|
| 1 | zagreb, monaco, cannes, brno, rome | ✓ | - | x | x | - | ✓ |
| 2 | bratislava, vienna, berlin, istanbul | - | ✓ | x | x | ✓  | - |
| 3 | 'marrakesh', 'budapest', 'budva', 'kotor', 'galway', 'dublin', 'rabat', 'montpellier', 'paris', 'nice', 'prague', 'podgorica', 'skopje', 'nis' | x | ✓ | - | ✓  | -  | - |
| 4 | 'brno', 'prague', 'munich', 'vienna', 'london', 'edinburgh' | - | x | - | -  | ✓  | x |
| 5 | 'brno', 'prague', 'bratislava', 'vienna', 'london', 'naples' | ✓ | - | - | ✓  | - |  - |
| 6 | 'cancun', 'mexico-city', ..., 'brno' | x | ✓ | x | x  | x  | - |
| 7 | 'london', 'lyon', 'dublin', 'prague', 'brno' | x | ✓ | ✓ | -  |-  | ✓ |
| 8 | 'budapest', 'cancun', 'dublin', 'berlin', 'turin' | - | x | ✓ | ✓  | - |  - |
| 6 | prague, berlin, copenhagen | x | ✓ | x | ✓  | -  | - |
| 7 | helsinki, oslo, stockholm | x | ✓ | x | ✓  | ✓  | ✓ |
| 8 | tokyo, sapporo, sydney | - | ✓ | x | -  | x |  ✓ |
| **score** | - | **-3** | **5** | **-4** | **2** | **1** | **3** |

## 6. Conclusion

### What we learned
- how to get, clean data, deal with missing values
- simple distance-based / clustering approaches work if there are meaningful features
- SVD and other 'black-box' collaborative-filtering technques tend to work better, but interpretability is worse
- improved our geographical knowledge

### What wasn't fnished
- application not run on a dedicated server, only locally
- more characteristic features of the cities weren't added (e.g. by scraping corresponding wiki pages, extracting what's each city known for)
- some features in the trips dataset were ignored