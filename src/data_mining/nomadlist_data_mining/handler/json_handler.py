import json


class JsonHandler:
    @staticmethod
    def load(filepath: str) -> dict:
        """
        Loads json from disk
        :param filepath: path to json file (with filename)
        :return: json as dict
        """
        with open(filepath, 'r') as jsonFile:
            json_as_string = jsonFile.read()
        jsonFile.close()
        return json.loads(json_as_string)

    @staticmethod
    def save(filepath: str, json_as_dict: dict) -> None:
        """
        Saves json into disk
        :param filepath: path to save json file (with filename)
        :param json_as_dict: dict
        :return: None
        """
        json_as_string = json.dumps(json_as_dict, indent=4)
        with open(filepath, "w+") as jsonFile:
            jsonFile.write(json_as_string)
        jsonFile.close()
