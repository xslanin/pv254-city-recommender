from typing import List, Dict

from src.data_mining.nomadlist_data_mining import CsvHandler
from src.data_mining.nomadlist_data_mining import JsonHandler
from src.data_mining.nomadlist_data_mining.scraper import Scraper

base_url = "https://nomadlist.com/"


class NomadCityCrawler:
    attribute_to_find = "data-key"
    attribute_to_find_values = [
            "hospital_availability",
            "nightlife",
            "female_friendly",
            "racial_tolerance",
            "walkability",
            "quality_of_life",
            "places_to_work_from",
            "friendliness_to_foreigners",
            "freedom_of_speech",
            "startup_score",
            "traffic_safety",
            "ac_availability",
            "lgbt_friendly",
            "fun",
            "wifi_availability",
            "peace",
            "nomad_score",
            "internet_mbs",
            "cost_dollars_per_month"]

    @staticmethod
    def get_number(string: str):
        inte = ""
        for c in string:
            if c.isdigit():
                inte += c
        return int(inte)

    @staticmethod
    def get_city_statistics(city: str) -> Dict[str, str]:
        full_url = base_url + city
        try:
            page = Scraper.get("https://nomadlist.com/" + city)
        except Exception as e:
            print("Web error for " + full_url)
            print(e)
            return {}

        values = {}
        if page.status_code() != 200:
            print("Unable to locate page for " + full_url)
            return values
        else:
            print("Working on " + full_url)

        data_value_stats = [
            ("hospital_score", "hospital_availability"),
            ("nightlife", "nightlife"),
            ("female_friendly", "female_friendly"),
            ("racial_tolerance", "racial_tolerance"),
            ("walkScore_score", "walkability"),
            ("life_score", "quality_of_life"),
            ("places_to_work_score", "places_to_work_from"),
            ("friendliness_to_foreigners", "friendliness_to_foreigners"),
            ("press_freedom_index_score", "freedom_of_speech"),
            ("startup_score", "startup_score"),
            ("road_traffic_score", "traffic_safety"),
            ("ac_availability", "ac_availability"),
            ("lgbt_friendly", "lgbt_friendly"),
            ("leisure_quality", "fun"),
            ("wifi_availability", "wifi_availability"),
            ("peace_score", "peace")]

        for key, mapping in data_value_stats:
            score = page.find().with_attribute("data-key", key).get_attribute_value("data-value")
            values[mapping] = score if score is not None else -1

        score = page.find().with_attribute("xitemprop", "ratingValue").get_text()
        values["nomad_score"] = score if score else -1

        text = "Mbps"
        score = page.find().contains_text(text).get_text() #page.find().get_soup().find(text=re.compile('.*' + text + '.*'))
        score = NomadCityCrawler.get_number(score)
        values["internet_mbs"] = score if score else -1

        text = " / mo"
        score = page.find().contains_text(text).get_text() #page.find().get_soup().find(text=re.compile('.*' + text + '.*'))
        score = NomadCityCrawler.get_number(score)
        values["cost_dollars_per_month"] = score if score else -1

        return values

    @staticmethod
    def get_cities_statistics(cities: List[str]):
        cities_result = {}
        cities_result["cities"] = {}
        for city in cities:
            try:
                result = NomadCityCrawler.get_city_statistics(city)
                if result:
                    cities_result["cities"][city] = result
            except Exception as e:
                print("Web error for " + city)
                print(e)

        return cities_result


def main():
    cities = []
    for line in CsvHandler.load("./../../data/trips.csv")[1]:
        cities.append(line["city"].lower().replace(" ", "-"))

    cities = list(dict.fromkeys(cities))
    print(cities)
    cities_info = NomadCityCrawler.get_cities_statistics(cities)
    JsonHandler.save("./../../data/cities_stats_full.json", cities_info)
    cities = JsonHandler.load("./../../data/cities_stats_full.json")
    CsvHandler.saveFromDict("./../../data/cities_stats_full.csv", "city", NomadCityCrawler.attribute_to_find_values,
                            cities["cities"], "-1")


main()
