from src.algorithms.recommender import *
from src.algorithms.helpers import *

class RandomSimilarity():

    def __init__(self):
        trips, cities = load_data("data/")
        self.trips = trips
        self.cities = cities
        self.cities_i = impute_missing_values(cities)
        self.city_dict = get_index_city_dict(cities)

    def random_similarity(self, visited_cities, n=5, display_max=5):
        results = self.cities.sample(n)['city']
        results_list = []
        for v in results.values:
            results_list.append([v,"0.99","N/A"])
        return results_list