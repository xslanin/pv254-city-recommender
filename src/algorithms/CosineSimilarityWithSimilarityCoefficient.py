from src.algorithms.recommender import *
from src.algorithms.helpers import *

from src.algorithms.helpers import load_data


class CosineSimilarityWithSimilarityCoefficient():

    def __init__(self):
        trips, cities = load_data("data/")
        self.trips = trips
        self.cities = cities
        self.cities_i = impute_missing_values(cities)
        self.cities_iter_i = iterative_impute_missing_values(cities)
        self.city_dict = get_index_city_dict(cities)

    def similarity_on_raw_data(self, visited_cities, n=5, display_max=5):
        return get_similar_cities_similarity_coeff_cosine(self.cities, self.trips, self.city_dict, visited_cities, n, display_max)

    def similarity_on_imputed_data(self, visited_cities, n=5,display_max=5):
        return get_similar_cities_similarity_coeff_cosine(self.cities_i, self.trips, self.city_dict, visited_cities, n, display_max)

    def similarity_on_iterative_imputed_data(self, visited_cities, n=5,display_max=5):
        return get_similar_cities_similarity_coeff_cosine(self.cities_iter_i, self.trips, self.city_dict, visited_cities, n, display_max)

    def similarity_on_binned_data(self, visited_cities, n=5, display_max=5):
        binned = prepare_for_binning(self.cities_i)
        return get_similar_cities_similarity_coeff_cosine(binned, self.trips, self.city_dict, visited_cities, n, display_max)