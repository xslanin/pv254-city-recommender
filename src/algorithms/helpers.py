import pandas as pd
import numpy as np
from sklearn.impute import SimpleImputer
from sklearn.experimental import enable_iterative_imputer
from sklearn.impute import IterativeImputer

def load_data(folder="../data/"):
    df_trips = pd.read_csv(f"{folder}trips.csv", sep='\s*,\s*', encoding='utf-8')
    df_trips.city = pd.Series([ str(city).lower().replace(' ', '-') for city in df_trips.city.values])
    df_trips["trip_duration"] = pd.to_timedelta(df_trips["trip_duration"])
    df_cities = pd.read_csv(f"{folder}cities_stats_full.csv", encoding='utf-8')
    return df_trips, df_cities

def impute_missing_values(cities, strategy="most_frequent"):
    cities = cities.replace(-1,np.NaN)
    imp = SimpleImputer(missing_values=np.nan, strategy=strategy)
    imp.fit(cities.drop(["city"], axis=1))
    imputed = imp.transform(cities.drop(["city"], axis=1))

    imputed = np.array(imputed, dtype=object)
    imputed = np.insert(imputed, 0, values=cities.city.values, axis=1)
    cities_imp = pd.DataFrame(data=imputed, index=cities.index, columns=cities.columns)
    return cities_imp

def iterative_impute_missing_values(df_cities):
    imp = IterativeImputer(max_iter=10, random_state=0, missing_values=-1)
    imputed = imp.fit_transform(df_cities.drop(['city'], axis=1))
    imputed.shape
    df_cities[["city"]]
    df_cities.loc[:, df_cities.columns != "city"] = imputed
    return df_cities

def prepare_for_binning(cities_i):
    to_bin_features = {"internet_mbs":[0, 10, 20, 30, 40, 104],
                       "cost_dollars_per_month": [0, 1000, 2000, 3000, 4000, 407344]}
    cities_ib = cities_i.drop([407]) # dropping mars-city -> outlier for cost_dollars_per_month
    for f,v in to_bin_features.items():
        cities_ib = bin_continuous_column(cities_ib, f, v, 5)
    return cities_ib.drop(to_bin_features, axis=1)

def bin_continuous_column(cities, feature, values, n_of_bins=5):
    bins = values
    labels = [x for x in range(n_of_bins)]
    cities[feature + '_bins'] = pd.cut(cities[feature], bins, labels=labels, include_lowest=True)
    return cities
