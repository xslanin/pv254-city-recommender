from sklearn.cluster import KMeans
from collections import Counter
from algorithms.recommender import *
from algorithms.helpers import *


class KNN():

    def __init__(self):
        trips, cities = load_data("data/")
        self.trips = trips
        self.cities = cities
        self.cities_i = impute_missing_values(cities)

    def similarity_on_inputed_data(self, visited_cities, n=5, display_max=5):
        clustering = KMeans(n_clusters=120,random_state=0).fit(self.cities_i.drop(['city'], axis=1))
        self.cities.loc[:,'labels'] = clustering.labels_

        city_labels = self.cities[self.cities["city"].isin(visited_cities)]["labels"]

        recommendations = self.cities[self.cities["labels"].isin(city_labels)].sample(n=n+len(visited_cities))['city'].tolist()
        
        results = []
        counter = 0
        for r in recommendations:
            if r not in visited_cities:
                counter += 1
                results.append([r, "N/A", "N/A"])
            if counter >= n:
                break

        return results