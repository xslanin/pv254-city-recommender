from sklearn.metrics.pairwise import cosine_similarity
from scipy.spatial.distance import minkowski, euclidean, braycurtis
from scipy.spatial.distance import jaccard, canberra, hamming
from sklearn.metrics import pairwise_distances

import operator
import pandas as pd
import numpy as np

def create_interaction_matrix(df, user_col, city_col, rating_col):
    """Create an interaction matrix dataframe.
        - df = Pandas DataFrame containing user-city interactions
        - user_col = column name containing user's identifier
        - city_col = column name containing city's identifier
    Output - 
        - df with user-city interactions
    """
    return df.groupby([user_col, city_col])[rating_col].sum().unstack().reset_index().fillna(0).set_index(user_col)

def get_index_city_dict(df):
    cities_dict = df[["city"]].to_dict()["city"]
    # now i have index:city, i want city:index
    inv_map = {v: k for k, v in cities_dict.items()}
    return inv_map

def get_reverse_index_city_dict(df):
    cities_dict = df[["city"]].to_dict()["city"]
    return cities_dict

def calculate_cosine_similarity(features):
    return cosine_similarity(features, features)

def cos_similarity(df, idx, how_many=10, drop=[]):
    drop.append("city")
    features = df.drop(drop, axis=1)
    similarity = calculate_cosine_similarity(features)
    sim_scores = list(enumerate(similarity[idx]))
    sim_scores = sorted(sim_scores, key=lambda x: x[1], reverse=True)
    if how_many != -1:
        sim_scores = sim_scores[1:1+how_many]
    indices = [i[0] for i in sim_scores]
    d = dict(zip(df['city'].iloc[indices].values, [float(i[1]) for i in sim_scores]))
    return sorted(d.items(), key=operator.itemgetter(1), reverse=True)

def similarity(df, idx, how_many=10, drop=[], sim='euclidean'):
    drop.append("city")
    features = df.drop(drop, axis=1)
    if sim == 'euclidean':
        similarity = pairwise_distances(features, features, metric='euclidean')
    if sim == 'minkowski':
        similarity = pairwise_distances(features, features, metric='minkowski')
    if sim == 'braycurtis':
        similarity = pairwise_distances(features, features, metric='braycurtis')
    if sim == 'hamming':
        similarity = pairwise_distances(features, features, metric='hamming')
    if sim == 'l1':
        similarity = pairwise_distances(features, features, metric='l1')
    if sim == 'manhattan':
        similarity = pairwise_distances(features, features, metric='manhattan')
    sim_scores = list(enumerate(similarity[idx]))
    sim_scores = sorted(sim_scores, key=lambda x: x[1], reverse=False)
    if how_many != -1:
        sim_scores = sim_scores[1:1+how_many]
    indices = [i[0] for i in sim_scores]
    d = dict(zip(df['city'].iloc[indices].values, [i[1] for i in sim_scores]))
    return sorted(d.items(), key=operator.itemgetter(1), reverse=False)

def get_city_count(df, A):
    """ Calculate how many times city A occured in a dataset
    """
    return df[df["city"] == A].shape[0]

def get_cities_overlap(df, A, B):
    """ Calculate how many times cities A and B occured together per 1 user
    """
    select_A = df[df["city"] == A]; select_B = df[df["city"] == B]
    users_per_A = set(select_A.user.values)
    users_per_B = set(select_B.user.values)
    shape_A = select_A.shape[0]; shape_B = select_B.shape[0]
    if len(users_per_A) > len(users_per_B):
        return len(users_per_A.intersection(users_per_B)), shape_A, shape_B
    else:
        return len(users_per_B.intersection(users_per_A)), shape_A, shape_B

def sim_coefficient(df, A, B):
    overlap, count_A, count_B = get_cities_overlap(df, A, B)
    return overlap/(count_A*count_B)

def random_similarity(df, visited_cities, n=5, display_max=5):
    results = df.cities.sample(n)['city']
    return results.values

def get_similar_cities_cosine(df, city_dict, cities, n_of_matches, n_to_display=None, metric='cosine'):
    """ Returns similarity coefficients for list of recommended cities 
    calculated by cosine similarity in descending order with reference to 
    what city lead to the recommendation 
    """
    d = []
    recommender_cities = []
    for c in cities:
        cos_sim = cos_similarity(df, city_dict[c], n_of_matches)
        for sim in cos_sim:
            if sim[0] in recommender_cities:
                # if city in recommended, keep the higher recommendation score
                if d[recommender_cities.index(sim[0])][1] < sim[1]:
                    d[recommender_cities.index(sim[0])][1] = sim[1]
                    d[recommender_cities.index(sim[0])][2] = c
            elif not sim[0] in cities:
                d.append([sim[0], sim[1], c])
                recommender_cities.append(sim[0])
    d = np.array(d)
    # sort by similarity value, [::-1] for reverse sort
    sorted_d = d[d[:, 1].argsort()][::-1]
    if n_to_display:
        return sorted_d[:n_to_display]
    else:
        return sorted_d

def get_similar_cities(df, city_dict, cities, n_of_matches, n_to_display=None, metric='euclidean'):
    """ Returns similarity coefficients for list of recommended cities 
    calculated by cosine similarity in descending order with reference to 
    what city lead to the recommendation 
    """
    d = []
    recommender_cities = []
    for c in cities:
        calculated_sim = similarity(df, city_dict[c], n_of_matches, [], metric)
        #print(calculated_sim[0])
        for sim in calculated_sim:
            if sim[0] in recommender_cities:
                # if city in recommended, keep the higher recommendation score
                if d[recommender_cities.index(sim[0])][1] > np.float64(sim[1]):
                    d[recommender_cities.index(sim[0])][1] = np.float64(sim[1])
                    d[recommender_cities.index(sim[0])][2] = c
            elif not sim[0] in cities:
                d.append([sim[0], np.float64(sim[1]), c])
                recommender_cities.append(sim[0])
    d = np.array(d)
    sorted_d = d[np.float64(d[:, 1]).argsort()]
    if n_to_display:
        return sorted_d[:n_to_display]
    else:
        return sorted_d

def get_similar_cities_similarity_coeff(df, df_trips, city_dict, cities, n_of_matches, n_to_display=None, metric='euclidean'):
    """ Returns similarity coefficients for list of recommended cities 
    calculated by cosine similarity in descending order with reference to 
    what city lead to the recommendation 
    """
    d = []
    recommender_cities = []
    for c in cities:
        calculated_sim = similarity(df, city_dict[c], n_of_matches, sim=metric)
        for sim in calculated_sim:
            if not sim[0] in cities:
                sim_c = sim_coefficient(df_trips, sim[0], c)
                if sim[0] in recommender_cities:
                    # if city in recommended, keep the higher recommendation score
                    if d[recommender_cities.index(sim[0])][1] > sim_c:
                        d[recommender_cities.index(sim[0])][1] = sim_c
                        d[recommender_cities.index(sim[0])][2] = c
                elif not sim[0] in cities:
                    d.append([sim[0], sim_c, c])
                    recommender_cities.append(sim[0])
    d = np.array(d)
    sorted_d = d[np.float64(d[:, 1]).argsort()][::-1]
    if n_to_display:
        return sorted_d[:n_to_display]
    else:
        return sorted_d

def get_similar_cities_similarity_coeff_cosine(df, df_trips, city_dict, cities, n_of_matches, n_to_display=None):
    """ Returns similarity coefficients for list of recommended cities 
    calculated by cosine similarity in descending order with reference to 
    what city lead to the recommendation 
    """
    d = []
    recommender_cities = []
    for c in cities:
        cos_sim = cos_similarity(df, city_dict[c], n_of_matches)
        for sim in cos_sim:
            if not sim[0] in cities:
                sim_c = sim_coefficient(df_trips, sim[0], c)
                if sim[0] in recommender_cities:
                    # if city in recommended, keep the higher recommendation score
                    if d[recommender_cities.index(sim[0])][1] < sim_c:
                        d[recommender_cities.index(sim[0])][1] = sim_c
                        d[recommender_cities.index(sim[0])][2] = c
                elif not sim[0] in cities:
                    d.append([sim[0], sim_c, c])
                    recommender_cities.append(sim[0])
    d = np.array(d)
    sorted_d = d[d[:, 1].argsort()][::-1]
    if n_to_display:
        return sorted_d[:n_to_display]
    else:
        return sorted_d

def get_similar_cities_sort_by_nomad_score(df, city_dict, cities, n_of_matches, n_to_display=None):
    """ Returns similarity coefficients for list of recommended cities 
    calculated by cosine similarity in descending order with reference to 
    what city lead to the recommendation 
    """
    d = []
    recommender_cities = []
    for c in cities:
        cos_sim = cos_similarity(df, city_dict[c], n_of_matches)
        for sim in cos_sim:
            if sim[0] in recommender_cities:
                # if city in recommended, keep the higher recommendation score
                if d[recommender_cities.index(sim[0])][1] < sim[1]:
                    d[recommender_cities.index(sim[0])][1] = sim[1]
                    d[recommender_cities.index(sim[0])][2] = c
            elif not sim[0] in cities:
                d.append([sim[0], sim[1], c])
                recommender_cities.append(sim[0])
    d = np.array(d)
    nomad_scores = []
    c = 0
    for d_ in d:
        nomad_scores.append(np.append(d[c], df[df["city"] == d_[0]]["nomad_score"].values[0]))
        c += 1
    d = np.array(nomad_scores)
    sorted_d = d[d[:, 3].argsort()][::-1]
    if n_to_display:
        return sorted_d[:n_to_display]
    else:
        return sorted_d

