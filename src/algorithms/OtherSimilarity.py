from src.algorithms.recommender import *
from src.algorithms.helpers import *

class OtherSimilarity():

    def __init__(self, metric="hamming"):
        trips, cities = load_data("data/")
        self.trips = trips
        self.cities = cities
        self.cities_i = impute_missing_values(cities)
        self.city_dict = get_index_city_dict(cities)
        self.metric = metric

    def similarity_on_raw_data(self, visited_cities, n=5, display_max=5):
        return get_similar_cities(self.cities, self.city_dict, visited_cities, n, display_max, metric=self.metric)

    def similarity_on_imputed_data(self, visited_cities, n=5,display_max=5):
        return get_similar_cities(self.cities_i, self.city_dict, visited_cities, n, display_max,  metric=self.metric)

    def similarity_on_binned_data(self, visited_cities, n=5, display_max=5):
        binned = prepare_for_binning(self.cities_i)
        return get_similar_cities(binned, self.city_dict, visited_cities, n, display_max, metric=self.metric)