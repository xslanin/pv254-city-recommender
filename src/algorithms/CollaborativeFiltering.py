from src.algorithms.recommender import *
from src.algorithms.helpers import *
from scipy.sparse.linalg import svds 
#from recommender import *
#from helpers import *

class CollaborativeFiltering():

    def __init__(self, data_path="data/"):
        trips, cities = load_data(data_path)
        self.trips = trips
        self.cities = cities
        self.cities_i = impute_missing_values(cities)
        self.city_dict = get_index_city_dict(cities)

    def recommend_for_user(self, user, num_of_items, R_df, user_id_dict, preds_df = None, model = None):
        n_users, n_items = R_df.shape

        user_row_numbers = user_id_dict[user]
        if preds_df is not None:
            sorted_user_predictions = preds_df.iloc[user_row_numbers].sort_values(ascending=False)
        elif model:
            scores = pd.Series(model.predict(user_id_dict[user],np.arange(n_items)))
            scores.index = R_df.columns
            sorted_user_predictions = scores.sort_values(ascending=False)
        
        scores_list = list(pd.Series(sorted_user_predictions.index))
        known_items = list(pd.Series(R_df.loc[user,:][R_df.loc[user,:] > 0].index).sort_values(ascending=False))

        scores_list = [x for x in scores_list if x not in known_items]
        city_rating_dict = {}
        for city in scores_list[:num_of_items]:
            city_rating_dict[city] = sorted_user_predictions[sorted_user_predictions.index == city].values[0]
        return city_rating_dict

    def append_user_to_trips_data(self, visited_cities, username):
        df = self.trips.copy()
        for city in visited_cities:
            lat = df[df["city"] == city].tail(1)[:1].latitude.values[0]
            longit = df[df["city"] == city].tail(1)[:1].longitude.values[0]
            country = df[df["city"] == city].tail(1)[:1].country.values[0]
            vals = df.tail()[:1].values[0]
            vals[0] = vals[0] + 1
            vals[1] = username
            vals[2] = city
            vals[3] = country
            vals[-2] = lat; vals[-1] = longit
            df.loc[df.shape[0]] = vals
        return df

    def similarity_with_SVD(self, visited_cities, n=5, display_max=5):
        username="live_user"
        modified_trips = self.append_user_to_trips_data(visited_cities, username)
        count_series = modified_trips.groupby(['user', 'city']).size()
        df_user_city = count_series.to_frame(name = 'number_of_visits').reset_index()
        df_user_city["number_of_visits_log"] = np.log(df_user_city["number_of_visits"]) + 1
        R_df = df_user_city.pivot(index = 'user', columns ='city', values = 'number_of_visits_log').fillna(0)
        user_id_dict = {}
        for i in range(R_df.shape[0]):
            user_id_dict[R_df.iloc[i].name] = i
        R = R_df.as_matrix()
        user_ratings_mean = np.mean(R, axis = 1)
        R_demeaned = R - user_ratings_mean.reshape(-1, 1)
        U, sigma, Vt = svds(R_demeaned, k = 50)
        sigma = np.diag(sigma)
        all_user_predicted_ratings = np.dot(np.dot(U, sigma), Vt) + user_ratings_mean.reshape(-1, 1)
        preds_df = pd.DataFrame(all_user_predicted_ratings, columns = R_df.columns)
        results = self.recommend_for_user(username, display_max, R_df, user_id_dict, preds_df)
        results_list = []
        for k,v in results.items():
            results_list.append([k,v,"N/A"])
        return results_list
