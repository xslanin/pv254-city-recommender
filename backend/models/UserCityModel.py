from . import db


class UserCityModel(db.Model):
    __tablename__ = "userCity"

    id = db.Column(db.Integer, primary_key=True)
    user = db.Column(db.String(200))
    city = db.Column(db.String(200))
    country = db.Column(db.String(200))
    trip_start = db.Column(db.Date)
    trip_end = db.Column(db.Date)
    trip_duration = db.Column(db.Integer)
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)

    def __init__(self, data, user, city, country,
                 trip_start, trip_end, trip_duration, latitude, longitude):
        self.city = city
        self.user = user
        self.city = city
        self.country = country
        self.trip_start = trip_start
        self.trip_end = trip_end
        self.trip_duration = trip_duration
        self.latitude = latitude
        self.longitude = longitude
