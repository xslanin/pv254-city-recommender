from . import db


class DestinationModel(db.Model):
    __tablename__ = "destinations"
    # __abstract__ = True
    #id = db.Column(db.Integer, primary_key=True)
    city = db.Column(db.String(200), primary_key=True)
    hospital_availability = db.Column(db.Integer)
    nightlife = db.Column(db.Integer)
    female_friendly = db.Column(db.Integer)
    racial_tolerance = db.Column(db.Integer)
    walkability = db.Column(db.Integer)
    quality_of_life = db.Column(db.Integer)
    place_to_work_from = db.Column(db.Integer)
    friendliness_to_foreigners = db.Column(db.Integer)
    freedom_of_speech = db.Column(db.Integer)
    startup_score = db.Column(db.Integer)
    traffic_safety = db.Column(db.Integer)
    ac_availability = db.Column(db.Integer)
    lgbt_friendly = db.Column(db.Integer)
    fun = db.Column(db.Integer)
    wifi_availability = db.Column(db.Integer)
    peace = db.Column(db.Integer)
    nomad_score = db.Column(db.Float)
    internet_mbs = db.Column(db.Integer)
    cost_dollars_per_month = db.Column(db.Integer)

    def __init__(self, city, hospital, nightlife, female,
                 racial, walkability, quality_of_life, place_to_work_from, friendliness,
                 freedom_of_speech, startup_score, traffic_safety, ac_availability,
                 lgbt_friendly, fun, wifi_availability, peace, nomad_score, internet_mbs,
                 cost_dollars_per_month
                 ):
        self.city = city
        self.hospital_availability = hospital
        self.nightlife = nightlife
        self.female_friendly = female
        self.racial_tolerance = racial
        self.walkability = walkability
        self.quality_of_life = quality_of_life
        self.place_to_work_from = place_to_work_from
        self.friendliness_to_foreigners = friendliness
        self.freedom_of_speech = freedom_of_speech
        self.startup_score = startup_score
        self.traffic_safety = traffic_safety
        self.ac_availability = ac_availability
        self.lgbt_friendly = lgbt_friendly
        self.fun = fun
        self.wifi_availability = wifi_availability
        self.peace = peace
        self.nomad_score = nomad_score
        self.internet_mbs = internet_mbs
        self.cost_dollars_per_month = cost_dollars_per_month
