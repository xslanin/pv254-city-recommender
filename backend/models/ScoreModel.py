from . import db


class ScoreModel(db.Model):
    __tablename__ = "score"

    algorithm_name = db.Column(db.String(200), primary_key=True, nullable=False, unique=True)
    score = db.Column(db.Integer, nullable=False)

    def __init__(self, data, algorithm_name, score):
        self.algorithm_name = algorithm_name
        self.score = score
