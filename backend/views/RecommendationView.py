from http import HTTPStatus
from backend.models import db
from flask import json, render_template, request, redirect, Blueprint, Response
from ..models.UserCityModel import UserCityModel

recommendBlueprint = Blueprint('recommend', __name__, url_prefix='')


@recommendBlueprint.route('/recommend', methods=['GET'])
def recommendInput():
    return render_template("recommend.html")
