from http import HTTPStatus
from ..models.DestinationModel import DestinationModel
from flask import json, render_template, request, redirect, Blueprint

from . import app, db


@app.route("/", methods=["GET", "POST"])
def index():
    if request.method == "GET":
        destinations = DestinationModel.query.all()
        return render_template("index.html", destinations=destinations)
    else:
        tmpDst = TmpDest()
        new_destination = DestinationModel(content=tmpDst)

        try:
            db.session.add(new_destination)
            db.session.commit()
            return redirect("/")
        except:
            return "There was an issiue adding your task"


@app.route("/search", methods=["GET"])
def get_destination(dest_name):
    destination_content = request.form["content"]
    destination = DestinationModel.query.get(dest_name)
    return render_template("index.html", destinations=destination)
