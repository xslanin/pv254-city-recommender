from flask import app, Blueprint, request
from . import db

from backend.models.ScoreModel import ScoreModel

scoreBlueprint = Blueprint('score', __name__, url_prefix='')


@scoreBlueprint.route('/like', methods=['GET'])
def like():
    alg = request.args.get("alg")
    score = ScoreModel.query.get(alg)

    score.score = score.score + 1
    db.session.merge(score)
    db.session.commit()
    db.session.flush()

    return "OK"


@scoreBlueprint.route('/dislike', methods=['GET'])
def dislike():
    alg = request.args.get("alg")
    score = ScoreModel.query.get(alg)

    score.score = score.score - 1
    db.session.merge(score)
    db.session.commit()
    db.session.flush()
    return "OK"
