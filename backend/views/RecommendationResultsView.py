from flask import render_template, request, Blueprint
import os
import sys

from flask import render_template, request, Blueprint

from src.algorithms.CosineSimilarityWithSimilarityCoefficient import CosineSimilarityWithSimilarityCoefficient
from ..models.DestinationModel import DestinationModel

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "/../../src/")
from src.algorithms.CosineSimilarity import CosineSimilarity
from src.algorithms.CollaborativeFiltering import CollaborativeFiltering
from src.algorithms.RandomSimilarity import RandomSimilarity
from src.algorithms.TripSimilarity import TripSimilarity
from src.algorithms.OtherSimilarityWithSimilarityCoefficient import OtherSimilarityWithSimilarityCoefficient
from src.algorithms.KNN import KNN

recommendationsBlueprint = Blueprint(
    'recommendations', __name__, url_prefix='')

@recommendationsBlueprint.route('/recommend/results', methods=['POST'])
def recommendationsResults():
    cities = request.form.getlist("city_name")
    result = DestinationModel.query.all()
    available_cities = [str(r).split(" ")[1].split(">")[0] for r in result]
    input_cities = []
    for c in cities:
        if c in available_cities:
            input_cities.append(c)

    cosine = CosineSimilarity()
    cosine_sim = CosineSimilarityWithSimilarityCoefficient()
    collab = CollaborativeFiltering()
    random = RandomSimilarity()
    knn = KNN()
    other_sim_hamming = OtherSimilarityWithSimilarityCoefficient('hamming')
    trip_sim = TripSimilarity()

    masking_dict = {
        "KNN clustering imputed data": "Recommendation 1",
        "SVD": "Recommendation 2",
        "Random": "Recommendation 3",
        "Cosine Raw data with SimilarityCoefficient": "Recommendation 4",
        "TripSimilarity": "Recommendation 5",
        "Hamming binned data with SimilarityCoefficient": "Recommendation 6"
    }

    if len(input_cities) != 0:
        recommendations = {
            masking_dict["KNN clustering imputed data"]: knn.similarity_on_inputed_data(input_cities),
            masking_dict["SVD"]: collab.similarity_with_SVD(input_cities),
            masking_dict["Random"]: random.random_similarity(input_cities),
            masking_dict["Cosine Raw data with SimilarityCoefficient"]: cosine_sim.similarity_on_raw_data(input_cities),
            masking_dict["TripSimilarity"]: trip_sim.get_recommendations_with_removal(input_cities),
            masking_dict["Hamming binned data with SimilarityCoefficient"]: other_sim_hamming.similarity_on_binned_data(input_cities)
        }
    else:
        recommendations = {}
    return render_template("recommend.html", results=recommendations, cities=input_cities)
