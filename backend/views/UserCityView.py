from http import HTTPStatus
from backend.models import db
from flask import json, render_template, request, redirect, Blueprint, Response

from backend.models.UserCityModel import UserCityModel
from ..models.DestinationModel import DestinationModel

userCityBlueprint = Blueprint('usercity', __name__, url_prefix='')


@userCityBlueprint.route('/ratings', methods=['GET'])
def getRatings():
    # if request.method == "POST":
    #     destination = request.form["content"]
    #     new_destination = DestinationModel(data=destination)
    # print(db)
    # print(new_destination)

    # try:
    #     db.session.add(new_destination)
    #     db.session.commit()
    #     return redirect("/")
    # except:
    #     return "There was an issiue adding your task"

    destinations = UserCityModel.query.limit(10).all()
    return render_template("userCity.html", destinations=destinations)
    # for destination in destinations:
    #     print(destination.id)
