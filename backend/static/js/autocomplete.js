function autocomplete(inp, arr) {
  //alert("{{city}}");
  /*the autocomplete function takes two arguments,
  the text field element and an array of possible autocompleted values:*/
  var currentFocus;
  //for (let inp in inps) {
      /*execute a function when someone writes in the text field:*/
      inp.addEventListener("input", function(e) {
          var a, b, i, val = this.value;
          /*close any already open lists of autocompleted values*/
          closeAllLists();
          if (!val) { return false;}
          currentFocus = -1;
          /*create a DIV element that will contain the items (values):*/
          a = document.createElement("DIV");
          a.setAttribute("id", this.id + "autocomplete-list");
          a.setAttribute("class", "autocomplete-items");
          /*append the DIV element as a child of the autocomplete container:*/
          this.parentNode.appendChild(a);
          /*for each item in the array...*/
          for (i = 0; i < arr.length; i++) {
          /*check if the item starts with the same letters as the text field value:*/
          if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
              /*create a DIV element for each matching element:*/
              b = document.createElement("DIV");
              /*make the matching letters bold:*/
              b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
              b.innerHTML += arr[i].substr(val.length);
              /*insert a input field that will hold the current array item's value:*/
              b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
              /*execute a function when someone clicks on the item value (DIV element):*/
                  b.addEventListener("click", function(e) {
                  /*insert the value for the autocomplete text field:*/
                  inp.value = this.getElementsByTagName("input")[0].value;
                  /*close the list of autocompleted values,
                  (or any other open lists of autocompleted values:*/
                  closeAllLists();
              });
              a.appendChild(b);
          }
          }
      });
      /*execute a function presses a key on the keyboard:*/
      inp.addEventListener("keydown", function(e) {
          var x = document.getElementById(this.id + "autocomplete-list");
          if (x) x = x.getElementsByTagName("div");
          if (e.keyCode == 40) {
          /*If the arrow DOWN key is pressed,
          increase the currentFocus variable:*/
          currentFocus++;
          /*and and make the current item more visible:*/
          addActive(x);
          } else if (e.keyCode == 38) { //up
          /*If the arrow UP key is pressed,
          decrease the currentFocus variable:*/
          currentFocus--;
          /*and and make the current item more visible:*/
          addActive(x);
          } else if (e.keyCode == 13) {
          /*If the ENTER key is pressed, prevent the form from being submitted,*/
          e.preventDefault();
          if (currentFocus > -1) {
              /*and simulate a click on the "active" item:*/
              if (x) x[currentFocus].click();
          }
          }
      });
      function addActive(x) {
      /*a function to classify an item as "active":*/
      if (!x) return false;
      /*start by removing the "active" class on all items:*/
      removeActive(x);
      if (currentFocus >= x.length) currentFocus = 0;
      if (currentFocus < 0) currentFocus = (x.length - 1);
      /*add class "autocomplete-active":*/
      x[currentFocus].classList.add("autocomplete-active");
      }
      function removeActive(x) {
      /*a function to remove the "active" class from all autocomplete items:*/
      for (var i = 0; i < x.length; i++) {
          x[i].classList.remove("autocomplete-active");
      }
      }
      function closeAllLists(elmnt) {
      /*close all autocomplete lists in the document,
      except the one passed as an argument:*/
      var x = document.getElementsByClassName("autocomplete-items");
      for (var i = 0; i < x.length; i++) {
          if (elmnt != x[i] && elmnt != inp) {
          x[i].parentNode.removeChild(x[i]);
      }
      }
  }
  /*execute a function when someone clicks in the document:*/
  document.addEventListener("click", function (e) {
      closeAllLists(e.target);
  });
 // }
}

/*An array containing ingredients*/
var ingredients = ['ho-chi-minh-city', 'seoul', 'alicante', 'amsterdam', 'london',
     'nha-trang', 'da-nang', 'mui-ne', 'paris', 'hong-kong',
     'chiang-mai', 'ko-samui', 'bangkok', 'bilbao', 'madrid',
     'barcelona', 'nottingham', 'istanbul', 'new-york-city', 'tunis',
     'florence', 'vientiane', 'kuala-lumpur', 'sanur', 'siem-reap',
     'phnom-penh', 'sliema', 'kharkiv', 'venice', 'tenerife', 'hue',
     'hanoi', 'vienna', 'rotterdam', 'dusseldorf', 'innsbruck', 'cairo',
     'alexandria', 'riga', 'antwerp', 'antalya', 'rhodes', 'bodrum',
     'jerusalem', 'tel-aviv', 'reykjavik', 'tangier', 'fes',
     'marrakesh', 'casablanca', 'vancouver', 'chicago', 'busan',
     'osaka', 'canggu', 'san-francisco', 'las-vegas', 'reno',
     'portland', 'seattle', 'honolulu', 'tokyo', 'ko-tao', 'taipei',
     'kyoto', 'nicosia', 'milan', 'arnhem', 'rome', 'jeju-island',
     'denpasar', 'guangzhou', 'kuta', 'ubud', 'singapore',
     'los-angeles', 'nijmegen', 'berlin', 'bratislava', 'kosice',
     'cologne', 'kiev', 'brussels', 'edmonton', 'haarlem', 'the-hague',
     'tirana', 'zurich', 'bern', 'copenhagen', 'prague', 'budapest',
     'dublin', 'tilburg', 'porto', 'lisbon', 'gdansk', 'ghent',
     'abu-dhabi', 'shenzhen', 'shanghai', 'athens', 'oslo', 'calgary',
     'stockholm', 'katowice', 'utrecht', 'nairobi', 'frankfurt',
     'tromso', 'groningen', 'warsaw', 'rzeszow', 'munich', 'dortmund',
     'toronto', 'seville', 'detroit', 'minneapolis', 'eindhoven',
     'bergen', 'atlanta', 'new-orleans', 'houston', 'zwolle', 'nassau',
     'beijing', 'moscow', 'zandvoort', 'regina', 'denver',
     'newcastle-upon-tyne', 'edinburgh', 'orlando', 'tampa', 'oxford',
     'havana', 'mombasa', 'ottawa', 'lexington', 'des-moines', 'fargo',
     'grande-prairie', 'hamburg', 'nuremberg', 'bologna', 'siena',
     'lucca', 'bruges', 'indio', 'redding', 'dallas', 'santa-cruz',
     'nanaimo', 'guelph', 'yangshuo', 'guilin', 'chengdu', 'victoria',
     'salzburg', 'turin', 'exeter', 'bristol', 'cardiff',
     'santa-barbara', 'montreal', 'quebec-city', 'heidelberg', 'pisa',
     'monaco', 'nice', 'abbotsford', 'ogden', 'salt-lake-city',
     'washington', 'ashland', 'phoenix', 'sacramento', 'miami',
     'geneva', 'lugano', 'toulouse', 'granada', 'las-palmas',
     'makassar', 'valencia', 'cebu', 'jakarta', 'cairns', 'byron-bay',
     'brisbane', 'rio-de-janeiro', 'fortaleza', 'belo-horizonte',
     'cusco', 'arequipa', 'lima', 'baltimore', 'florianopolis',
     'leipzig', 'pristina', 'kotor', 'dubrovnik', 'mostar', 'essaouira',
     'kralendijk', 'mountain-view', 'dahab', 'san-sebastian', 'daejeon',
     'novi-sad', 'belgrade', 'krabi', 'doha', 'interlaken',
     'montpellier', 'birmingham', 'da-lat', 'george-town', 'kuching',
     'kaohsiung', 'pokhara', 'dhaka', 'kathmandu', 'tarifa', 'naha',
     'fukuoka', 'okayama', 'nagoya', 'hamamatsu', 'yangon', 'mandalay',
     'ao-nang', 'santiago', 'norwich', 'fort-lauderdale', 'cartagena',
     'phuket', 'chania', 'malaga', 'buenos-aires', 'san-juan-del-sur',
     'playa-del-carmen', 'montego-bay', 'puerto-viejo', 'panama-city',
     'vilnius', 'colombo', 'ko-lanta', 'sydney', 'aarhus', 'dubai',
     'hobart', 'sapporo', 'cancun', 'dresden', 'mexico-city',
     'guwahati', 'stamford', 'ann-arbor', 'helsinki', 'trondheim',
     'alesund', 'pittsburgh', 'austin', 'kigali', 'winnipeg', 'split',
     'hvar', 'zagreb', 'brighton', 'toledo', 'kochi', 'varkala',
     'kannur', 'mumbai', 'ahmedabad', 'jodhpur', 'delhi', 'pattaya',
     'addis-ababa', 'madison', 'cody', 'idaho-falls', 'bozeman',
     'casper', 'eugene', 'banff', 'antananarivo', 'milwaukee',
     'luang-prabang', 'sofia', 'buffalo', 'tallinn', 'gili-air',
     'sendai', 'tehran', 'shiraz', 'isfahan', 'bandar-seri-begawan',
     'kota-kinabalu', 'johor-bahru', 'nashville', 'boston', 'halifax',
     'santorini', 'male', 'muscat', 'jeddah', 'sa-pa', 'kolkata',
     'bhubaneswar', 'visakhapatnam', 'kagoshima', 'beirut', 'cali',
     'wilmington', 'hua-hin', 'davenport', 'grand-junction',
     'schenectady', 'new-haven', 'glasgow', 'southampton',
     'johannesburg', 'cape-town', 'windhoek', 'livingstone', 'lilongwe',
     'khartoum', 'luxor', 'benghazi', 'tripoli', 'queenstown',
     'cambridge', 'christchurch', 'auckland', 'adelaide', 'melbourne',
     'ko-pha-ngan', 'richmond', 'pai', 'jaipur', 'agra', 'moradabad',
     'izmir', 'taos', 'flagstaff', 'tucson', 'reading', 'ulaanbaatar',
     'novosibirsk', 'almaty', 'bishkek', 'dushanbe', 'volgograd',
     'cluj', 'san-diego', 'bursa', 'fort-wayne', 'omaha', 'little-rock',
     'ushuaia', 'wellington', 'saint-petersburg', 'langkawi', 'tainan',
     'basel', 'mykonos', 'palma', 'boracay', 'surabaya', 'manila',
     'pyongyang', 'port-moresby', 'grenoble', 'york', 'hoi-an',
     'managua', 'oaxaca', 'malacca', 'goa', 'ko-phi-phi', 'montevideo',
     'naples', 'mendoza', 'manchester', 'santo-domingo', 'rosario',
     'cordoba', 'salta', 'la-paz', 'guayaquil', 'galapagos-islands',
     'quito', 'santa-marta', 'bogota', 'manaus', 'belem', 'sao-luis',
     'salvador', 'karlsruhe', 'odessa', 'zadar', 'marseille',
     'chamonix', 'santa-maria', 'mars-city', 'taichung', 'cannes',
     'ljubljana', 'arrecife', 'northampton', 'cabarete', 'sheffield',
     'aberdeen', 'palawan', 'port-louis', 'chiang-rai', 'el-gouna',
     'boulder', 'leeds', 'burgas', 'kandy', 'rishikesh', 'canberra',
     'oranjestad', 'poznan', 'philadelphia', 'worcester', 'baguio',
     'gold-coast', 'varanasi', 'bath', 'udon-thani', 'sihanoukville',
     'lviv', 'batumi', 'tbilisi', 'vang-vieng', 'nago', 'urumqi',
     'tashkent', 'chennai', 'brasov', 'bucharest', 'palermo',
     'seminyak', 'gibraltar', 'lagos', 'paphos', 'szczecin', 'funchal',
     'antigua', 'corralejo', 'jacksonville', 'gothenburg', 'chisinau',
     'isla-mujeres', 'bocas-del-toro', 'goiania', 'lille', 'strasbourg',
     'chatham', 'uluwatu', 'dili', 'punta-cana', 'minsk', 'coventry',
     'varna', 'belfast', 'bengaluru', 'baku', 'durban',
     'port-elizabeth', 'maputo', 'albuquerque', 'makati',
     'thiruvananthapuram', 'naypyidaw', 'hamilton', 'guadalajara',
     'wuhan', 'kunming', 'puerto-vallarta', 'charlotte', 'deventer',
     'taghazout', 'chandigarh', 'amritsar', 'yogyakarta', 'malang',
     'bandung', 'ipoh', 'ko-lipe', 'mataram', 'leeuwarden', 'ibiza',
     'sayulita', 'tamarindo', 'thessaloniki', 'avignon',
     'yekaterinburg', 'liberia', 'asuncion', 'brasilia', 'tartu',
     'shenyang', 'sinuiju', 'harbin', 'queretaro', 'guiyang', 'perth',
     'chongqing', 'gurgaon', 'indianapolis', 'road-town', 'tulum',
     'merida', 'whistler', 'kauai', 'bend', 'luanda', 'graz', 'huambo',
     'nantes', 'padova', 'barranquilla', 'aguascalientes', 'guanajuato',
     'puebla', 'osijek', 'skopje', 'coimbra', 'sarajevo', 'cork',
     'klaipeda', 'santa-teresa', 'rovaniemi', 'phu-quoc', 'hartford',
     'amalfi', 'cincinnati', 'menlo-park', 'palo-alto', 'cleveland',
     'oakland', 'luton', 'bari', 'fairfax', 'medford', 'lyon',
     'krasnodar', 'saratov', 'bar', 'hiroshima', 'lake-tahoe',
     'syracuse', 'jackson', 'helena', 'kingston', 'spokane', 'santa-fe',
     'memphis', 'asheville', 'lancaster', 'lake-como', 'park-city',
     'caye-caulker', 'timisoara', 'montanita', 'cuenca', 'budva',
     'daegu', 'luxembourg', 'bethlehem', 'lublin', 'puerto-escondido',
     'cabo-san-lucas', 'cavtat', 'stuttgart', 'salem', 'jamestown',
     'lafayette', 'bedford', 'shaoxing', 'hangzhou', 'lusaka',
     'dar-es-salaam', 'kampala', 'hohhot', 'zhangjiakou', 'nanning',
     'nanchang', 'changsha', 'suzhou', 'rennes', 'portsmouth', 'boise',
     'podgorica', 'plovdiv', 'medan', 'san-jose', 'vatican-city',
     'dakar', 'yerevan', 'andorra-la-vella', 'praia', 'a-coruna',
     'cagliari', 'minca', 'aachen', 'banja-luka', 'cheltenham',
     'liverpool', 'pune', 'bielefeld', 'okinawa-city', 'surat', 'genoa',
     'kansas-city', 'columbia', 'cozumel', 'brno', 'ningbo', 'yiwu',
     'fort-collins', 'raleigh', 'grand-rapids', 'panajachel', 'amman',
     'jarabacoa', 'suva', 'larnaca', 'gwangju', 'scranton',
     'burlington', 'columbus', 'san-juan', 'san-pedro-de-atacama',
     'antofagasta', 'savannah', 'anchorage', 'galway', 'fort-mcmurray',
     'moncton', 'san-pedro-sula', 'kaunas', 'limassol', 'charleston',
     'bordeaux', 'windsor', 'harrisonburg', 'darmstadt',
     'virginia-beach', 'haifa', 'arendal', 'kobe', 'kollam', 'madurai',
     'hyderabad', 'galle', 'napa', 'braga', 'ankara', 'bellingham',
     'san-salvador', 'huntsville', 'astana', 'tijuana', 'dayton',
     'lausanne', 'rabat', 'ajaccio', 'sarasota', 'key-west', 'wanaka',
     'general-santos', 'trieste', 'constanta', 'trento', 'rochester',
     'ithaca', 'gainesville', 'mancora', 'salento', 'aveiro',
     'ponta-delgada', 'xiamen', 'fort-myers', 'mysore', 'marbella',
     'mcleod-ganj', 'san-antonio', 'west-palm-beach', 'annecy',
     'beacon', 'bloomington', 'hilo', 'bansko', 'colorado-springs',
     'semarang', 'las-cruces', 'pamplona', 'lodz', 'yokohama',
     'roanoke', 'javea', 'patras', 'belize-city', 'aalborg',
     'bay-islands', 'monterey', 'nanjing', 'zhengzhou', 'luoyang',
     'jinan', 'dalian', 'qinhuangdao', 'tianjin', 'leiden', 'trier',
     'breda', 'banjul', 'princeton', 'biarritz', 'pretoria', 'erlangen',
     'bridgetown', 'monterrey', 'bremen', 'agadir', 'guatemala-city',
     'freetown', 'kelowna', 'san-miguel-de-allende', 'punta-del-este',
     'recife', 'providence', 'zhuhai', 'curitiba', 'campo-grande',
     'la-crosse', 'juarez', 'chiang-mai-thailand', 'davao', 'sochi',
     'hannover', 'rimini', 'xining', 'amarillo', 'tulsa', 'scottsdale',
     'oklahoma-city', 'san-clemente', 'carlsbad', 'rijeka', 'yichang',
     'willemstad', 'wenzhou', 'manhattan', 'chattanooga', 'caracas',
     'palm-bay', 'olympia', 'greenville', 'rouen', 'qingdao', 'manama',
     'oulu', 'temple', 'lake-balaton', 'louisville', 'philipsburg',
     'san-pedro', 'pensacola', 'shreveport', 'bowling-green',
     'warner-robins', 'bismarck', 'campinas', 'porto-alegre',
     'baton-rouge', 'lanzhou', 'wuhu', 'wollongong', 'texel', 'lincoln',
     'kearney', 'green-bay', 'ramallah', 'matara', 'kanpur', 'jimbaran',
     'missoula', 'verona', 'pueblo', 'tegucigalpa', 'ko-chang', 'nuuk',
     'hull', 'alanya', 'galveston', 'accra', 'yaounde', 'juba',
     'ulyanovsk', 'samara', 'kazan', 'chelyabinsk', 'sioux-falls',
     'beaumont', 'datong', 'dongguan', 'darwin', 'changzhou',
     'aurangabad', 'la-serena', 'yuma', 'leicester', 'vista',
     'kitchener', 'riverside', 'novocherkassk', 'zhanjiang', 'kassel',
     'joliet', 'albany', 'fuzhou', 'knoxville', 'surrey', 'conakry',
     'modena', 'nagpur', 'rostov-on-don', 'rethymno', 'temecula',
     'terre-haute', 'tallahassee', 'riyadh', 'nashua', 'wichita',
     'augusta', 'saskatoon', 'port-vila', 'ashgabat', 'iasi', 'nis',
     'bournemouth', 'shrewsbury', 'waterloo', 'pasto', 'noida',
     'ciutadella', 'georgetown', 'weston', 'san-ignacio', 'canton',
     'port-au-prince', 'springdale', 'bhopal', 'amundsen-scott',
     'leuven', 'kaesong', 'sharjah', 'visalia', 'charlotte-amalie',
     'dunedin', 'douala', 'sandpoint', 'joplin', 'springfield', 'provo',
     'kaliningrad', 'bradenton', 'gori', 'apple-valley', 'harare',
     'mamaia', 'dijon', 'newport-beach', 'karachi', 'duluth',
     'daytona-beach', 'greensboro', 'palembang', 'sorocaba', 'nizhny',
     'bucaramanga', 'turku', 'petaluma', 'palm-desert', 'port-of-spain',
     'ferrara', 'giza', 'bakersfield', 'lake-havasu-city', 'senggigi',
     'lansing', 'vladivostok', 'indore', 'lucknow'];


  let addMoreAuto = document.getElementsByName("city_name");
  addMoreAuto.forEach(el => {
      assignListeners(el);
  });

  function assignListeners(element) {
      autocomplete(element, ingredients);
      element.addEventListener("input", autoAdder);
      element.addEventListener("input", autoRemover);
  }

  function autoAdder() {
      let inputs = document.getElementsByName('city_name');
      let allFull = true;

      inputs.forEach(input => {
          if (input.value == null || input.value === ""){
              allFull = false
          }
      });

      if (allFull){
          document.getElementById("first").insertAdjacentHTML('beforeend', "<input class='autocomplete' class='form-control' type='text' name='city_name' placeholder='Enter a city'/>");
          let els = document.getElementsByName("city_name");
          let lastAdded = els[els.length - 1];
          assignListeners(lastAdded);
      }
  }

  function autoRemover() {
      let inputs = document.getElementsByName('city_name');

      let first = true;
      inputs.forEach(input => {
          if (input.value == null || input.value === ""){
              if (first){
                  first = false;
              }else{
                  input.parentNode.removeChild(input);
              }
          }
      });
  }