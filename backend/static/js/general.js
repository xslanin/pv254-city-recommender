
function createOption(){
    let newForm = document.createElement("OPTION");
}


function sendLike(elem, alg) {
    let request = new XMLHttpRequest();

    disableButtons(elem, "#b3ffe0")
    // Open a new connection, using the GET request on the URL endpoint
    request.open('GET', window.location.origin + '/like?alg=' + alg, true);

    // Send request
    request.send();
}

function sendDislike(elem, alg) {
    let request = new XMLHttpRequest();
    disableButtons(elem, "#bfbfbf")

    // Open a new connection, using the GET request on the URL endpoint
    request.open('GET', window.location.origin + '/dislike?alg=' + alg, true);

    // Send request
    request.send();

}

function disableButtons(elem, color){

    elem.disabled = true;
    elem.style.backgroundColor = color;
    elem.parentNode.parentNode.style.backgroundColor = color;
}

function showInfo(city){
    let iframe = document.getElementsByClassName("info_inside")[0];
    iframe.parentNode.removeChild(iframe);

    document.getElementsByClassName("info")[0].insertAdjacentHTML('beforeend', "<iframe class='info_inside' src='https://www.google.com/search?igu=1&ei=&q="+ city + "' frameborder='0' style='border:0' scrolling='no'></iframe>");

}