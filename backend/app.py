from flask import Flask, render_template, url_for, request, redirect
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
import csv

from backend.views.RecommendationAlgorithmScore import scoreBlueprint
from .models.UserCityModel import UserCityModel
from .models.DestinationModel import DestinationModel
from .models import db
from .views.UserCityView import userCityBlueprint
from .views.RecommendationView import recommendBlueprint
from .views.RecommendationResultsView import recommendationsBlueprint
from .config import app_config
import pandas as pd
import os
from flask_bootstrap import Bootstrap


# app = Flask(__name__, template_folder="templates", static_folder="static")
# /// - relative path //// - absolute path
# postgresql://username:passwd@localhost/naomeofthedatabase
# app.config[
#     "SQLALCHEMY_DATABASE_URI"
# ] = "postgresql://michalhornak:vancouver12@localhost/test_db"
# app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
# db = SQLAlchemy(app)


def create_app(env_name):
    app = Flask(__name__, template_folder="templates", static_folder="static")
    Bootstrap(app)
    app.config.from_object(app_config[env_name])
    # app.config[
    #     "SQLALCHEMY_DATABASE_URI"
    # ] = "postgresql://michalhornak:vancouver12@localhost/test_db"
    # db.init_app(app)

    app.register_blueprint(userCityBlueprint)
    app.register_blueprint(recommendBlueprint)
    app.register_blueprint(recommendationsBlueprint)
    app.register_blueprint(scoreBlueprint)

    with app.test_request_context():
        db.init_app(app)
        db.create_all()
        # csv_data = pd.read_csv(
        #    '/Users/michalhornak/Documents/School/MUNI/3. semester/PV254/recomander/pv254-city-recommender/backend/models/data/data_cities_stats_full.csv')
        # csv_data.to_sql(con=db, index_label='id',
        #                name='destinations', if_exists='replace')

    @app.route('/', methods=['GET'])
    def index():

        destinations = UserCityModel.query.limit(10).all()
        #return render_template("index.html", destinations=destinations)
        return redirect(url_for('recommend.recommendInput'))
    return app
