from wtforms import (
    Form,
    StringField,
    PasswordField,
    validators,
    SubmitField,
    SelectField,
)
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo, Length


class CreateDestination(Form):
    # [VARIABLE] = [FieldType]('[LABEL]', [
    #     validators.[VALIDATOR TYPE](message=('[VALIDATOR ERROR'))
    # ])
    city = StringField("City", [validators.DataRequired(message=("Don't be shy!"))])
    # hospital_score = StringField('Hospital score')
    # english_speaking = StringField('English speaking')
    submit = SubmitField("Add")

