# PV254-city-recommender

Project for PV254 (Fall 2019). Recommends a city based on your travel history.

# Motivation

## Problem statement
We're recommending cities to visit as travel destinations based on person's travel history and other preferences.
- **Use case:** I, as a user, want to find interesting cities to visit based on cities I liked in the past and my personal preferences (e.g. cost, peacefulness, etc.)

## Dataset
The dataset, found in the `data/` folder, is scraped from user pages of [nomadlist.com](www.nomadlist.com). Contains 72k trips, 3700 users and 599 cities.

## Algorithms
- cosine/other types of similarities on features of `cities` dataset
- combination with similarity coefficient
- combination with feature engineering (data imputing, faeture binning)

## Algorithms used in beta-testing
- SVD (treated as a blackbox and used as reference)
    - factorization technique using linear mapping between users : cities datasets, [more e.g. here](https://medium.com/@m_n_malaeb/singular-value-decomposition-svd-in-recommender-systems-for-non-math-statistics-programming-4a622de653e9)
- Random (for reference and as a baseline)
    - randomly sample `n` cities from `cities` dataset
- TripSimilarity using `trips` dataset

    0. Groups cities based on user
    1. Removes duplicities
    2. Finds groups that satisfies user input (has same cities in group or biggest subsection if no group with full satisfaction)
    3. From groups finds most cities with most occurrences (excluding user input)
    4. Shuffle in groups positions (to avoid discrimination based on position)
    5. Recommend first x (based on preferences)
- Minkowski distance on `cities` with imputed data, using SimilarityCoefficient on `trips` dataset

    0. Impute the data - replace `-1` as missing values by most frequent value in that row
    1. Calculate Minkowski distance with data (linear distance between 2 vectors) between input cities and all the other cities
    2. Choose `n` closest for each input city -> output_cities[n]
    3. For each input city calculate SimilarityCoefficient with its output cities
        - `sim(city_A, city_B) = count(any user was in city_A and city_B)/(count(any user was in city_A)*count(any user was in city_B))`
    4. Output `n` cities with highest SimilarityCoefficient
- Hamming distance on `cities` with binned imputed data, using SimilarityCoefficient on `trips` dataset

    0. Impute and bin the data - replace `-1` as missing values by most frequent value in that row and make all the values range between 1-5
    1. Calculate Hamming distance between input cities and all the other cities
        - Hamming calculates the number of positions at which the corresp. symbols differ
    2. Choose `n` closest for each input city -> output_cities[n]
    3. For each input city calculate SimilarityCoefficient with its output cities
        - `sim(city_A, city_B) = count(any user was in city_A and city_B)/(count(any user was in city_A)*count(any user was in city_B))`
    4. Output `n` cities with highest SimilarityCoefficient

- Cosine similarity on `cities` with raw data, using SimilarityCoefficient on `trips` dataset
    1. Calculate Cosine similarity between input cities and all the other cities
    2. Choose `n` closest for each input city -> output_cities[n]
    3. For each input city calculate SimilarityCoefficient with its output cities
        - `sim(city_A, city_B) = count(any user was in city_A and city_B)/(count(any user was in city_A)*count(any user was in city_B))`
    4. Output `n` cities with highest SimilarityCoefficient

- KNN with imputed values clustering using cities dataset:
    1. Imputes missing values of '-1' for the most frequent ones.
    2. Uses features stored in cities dataset to find n nearest neighbours clusters
    3. Find clusters of the known cities
    4. Sample those clusters for the output
    5. Output `n` random cities from the clusters of selected cities 
    

# How to run
- 1. navigate to the root directory of this repo
- 2. run `pip install -r requirements.txt`

# How to run the server
- 1. navigate to the root directory of this repo
- 2. run `pip install -r requirements.txt`

## Sqlite3 setup
- 3. Download and install sqlite3 and sqlitestudio
- 4. run `python run.py` // will fail, but creates empty tables
- 5. run sqlitestudio
- 6. import db file from project
- 7. find `app` DB in it and rightclick on `destinations`/ `userCity`, import data from `data/` into it

#### Sqlite3 console setup
- `sqlite3`
- .mode csv //nastavi imput/output na csv file
- .import path_to_the_trips_csv userCity
- .import path_to_the_cities_csv destinations

## Working with the datasets: See boilerplate code in src/Starter.ipynb
- 3. run `jupyter notebook`
- 4. select `src/Starter.ipynb`

# Progress

## To be done on 25.11. (see [Starter](https://gitlab.fi.muni.cz/xslanin/pv254-city-recommender/blob/master/src/Starter.ipynb)):
- assign responsibilities for:
- - come up with more approaches for recommendations using similarities (see [Approach 1 in Starter](https://gitlab.fi.muni.cz/xslanin/pv254-city-recommender/blob/master/src/Starter.ipynb)) - Jirka
- - dimensionality reduction / visualization of city distances using PCA/t-SNE - Martin
- - data analysis on cities dataset (partially started by Martin) - Martin
- - come up with more features for cities, e.g. sport, art, nature - Won't do
- - create 1 blackbox recommendation solution for comparison purposes - Terka
- - think about testing scenarios, focus on use cases - Terka
- - - e.g. "I've been to (generic city)", "I've been to (list of generic cities)", "I've been to (generic city and obscure one)", etc.
- - - e.g. "I care about (feature)", "I care about (list of similar features)", "I care about (list of disimilar features)", etc.
- - - combination: e.g. "I care about (feature) and i've been to (list of generic cities)" - won't 
- - - conditioning on a location: e.g. "I care about (feature) and i've been to (list of generic and obscure cities) and i want to go to (continent)" - won't
- - testing: come up with a dataset of testing users based on the use cases
- - run all the algorithms on it and come up with explanations (if there are)
- - webserver and final presentation of the work - Michal